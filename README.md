# weather

A Clojure library that returns the current weather conditions (type, description and temperature) for a given location

## Usage

(get-weather "city,country-code")
	example: 	(get-weather "london,uk")
				(get-weather "riga,lv")

## License

Copyright © 2017 Maris Jirgesn

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
