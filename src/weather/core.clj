(ns weather.core
  (:require [org.httpkit.client :as http]
            [cheshire.core :refer :all]))


(defn parse-weather[body]
  (def mapinsis (parse-string body true)) ;;parse response
  (def description (get-in mapinsis [:weather 0 :description])) ;;Get weather description
  (def type (get-in mapinsis [:weather 0 :main])) ;;Get main weather type
  (def temperature (- (get-in mapinsis [:main :temp]) 273.15)) ;;Convert from Kelvin to Celcius
  (def nameOfPlace (get-in mapinsis [:name]))
  (println "Current weather in " nameOfPlace ": " type
           "\nDescription of weather: " description
           "\nTemperature: "(format "%.2f" temperature) " degrees celcius")
)

(defn get-weather[location]
  ;;using my own api key for this. https was kind of out my league right now
  (def link (str "http://api.openweathermap.org/data/2.5/weather?q=" location "&APPID=c7660f785750d2d65d29cee76ae5a534"))
   (http/get link {:as :text}
          (fn [{:keys [status headers body error opts]}]
            (if error
              (println "Failed, exception is " error)
              (parse-weather body)
          )
        )
   )
)


(get-weather "london,uk")
